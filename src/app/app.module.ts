import { AuthService } from './../providers/auth/auth-service';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';

import { HttpModule } from '@angular/http';

import { Firebase } from '@ionic-native/firebase';

import { Chart } from 'chart.js';

import { Fire } from '../util/firebase.config';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SigninPage } from '../pages/signin/signin';
import { SignupPage } from '../pages/signup/signup';
import { ResetpasswordPage } from '../pages/resetpassword/resetpassword';
import { ProdutoPage } from '../pages/produto/produto';
import { ItensdesafioPage } from '../pages/itensdesafio/itensdesafio';
import { ProdutoProvider } from '../providers/produto/produto';

//import { ProdutoService } from '../providers/produto/produtoService';
//import { GooglePlus } from '@ionic-native/google-plus';
//import { Facebook } from '@ionic-native/facebook';
//import { TwitterConnect } from '@ionic-native/twitter-connect';
const firebaseConfig = {
  apiKey: "AIzaSyBipYAs6-scyXQ6lLrbxUWdLKBfc6LFVMs",
  authDomain: "bddesafio.firebaseapp.com",
  databaseURL: "https://bddesafio.firebaseio.com",
  projectId: "bddesafio",
  storageBucket: "bddesafio.appspot.com",
  messagingSenderId: "969159133129"
};


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SigninPage,
    SignupPage,
    ResetpasswordPage,
    ProdutoPage,
    ItensdesafioPage,

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    HttpModule,
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SigninPage,
    SignupPage,
    ResetpasswordPage,
    ProdutoPage,
    ItensdesafioPage,
    

  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthService,
    Firebase,
    ProdutoPage,
    ItensdesafioPage,
    ProdutoProvider
   /* GooglePlus,
    Facebook,
    TwitterConnect*/
  ]
})
export class AppModule {}