import { Component, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AuthService } from '../../providers/auth/auth-service';
import { AngularFireAuth } from 'angularfire2/auth';
import { SigninPage } from '../signin/signin';
import { ItensdesafioPage } from '../itensdesafio/itensdesafio';
import { Chart } from 'chart.js';
import { ProdutoPage } from '../produto/produto';




@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  displayName: string;
  imgUrl: string;
  @ViewChild('graficofeito') doughnutCanvas;

  doughnutChart: any;

  
  constructor(
    public navCtrl: NavController,
    private authService: AuthService,
    private afAuth: AngularFireAuth) {

    const authObserver = afAuth.authState.subscribe(user => {
      this.displayName = '';
      this.imgUrl = '';
      if (user) {
        this.displayName = user.displayName;
        this.imgUrl = user.photoURL;

        authObserver.unsubscribe();
      }
    });
  }

  ionViewDidLoad(){
   
    this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {
 
      type: 'doughnut',
      data: {
          labels: ["Obrigatorio", "Desejavel", "Não Aplicado"],
          datasets: [{
              label: ' Feito',
              data: [4, 3, 1  ],
              backgroundColor: [
                  'rgba(255, 99, 132, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(255, 206, 86, 0.2)'
              ],
              hoverBackgroundColor: [
                  "#FF6384",
                  "#36A2EB",
                  "#FFCE56"
              ]
          }]
      }

  })  }

  itenPage(){
      this.navCtrl.push(ItensdesafioPage);
  }
  
  produtoPage(){
    this.navCtrl.push(ProdutoPage);
  }

  signOut() {
    this.authService.signOut()
      .then(() => {
        this.navCtrl.setRoot(SigninPage);
      })
      .catch((error) => {
        console.error(error);
      });
  }

   signinTheApp(){
      this.navCtrl.setRoot(HomePage);
      //this.navCtrl.viewWillUnload(HomePage);
   }

}