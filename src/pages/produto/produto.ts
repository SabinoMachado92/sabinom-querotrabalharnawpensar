import { ProdutoProvider } from './../../providers/produto/produto';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, InfiniteScroll } from 'ionic-angular';
import { ViewChild } from '@angular/core';


@IonicPage()
@Component({
  selector: 'page-produto',
  templateUrl: 'produto.html',
})
export class ProdutoPage {

  produtos: any;
  alert: string;
  
  @ViewChild(InfiniteScroll) infiniteScroll: InfiniteScroll;

  constructor(public navCtrl: NavController, public navParams: NavParams, private toast: ToastController, private ProdutoProvider: ProdutoProvider) {
  }

        ngOnInit() {
          this.getProdutos();
        }

        // Essa é a parte principal:
        /*
          1. Esse método é executado logo ao abrir a página, pois é chamado no ngOnInit()
          2. Nós usamos o ProdutoProvider, que é o arquivo onde está fazendo a consulta na API
          3. O que alterei aqui foi que o this.ProdutoProvider retorna uma Promise, e ela é
             é composta por dois tipos de retorno, o THEN e o CATCH. Caso dê tudo certo na consulta
             ele cai no THEN, caso ocorra algum erro, ele cai no CATCH.
          4. No then nós pegamos o retorno da consulta, que nomeamos como produtos, e atribuímos
             a variável this.produtos, que corresponde ao produtos criado lá em cima, na inicialização
             da classe ProdutoPage.
          5. Automaticamente quando a variável de classe produtos é populada o angular renderiza a lista
             na tela.
          6. Caso ocorra algum erro, eu mostro o retorno do erro no console do browser.
          7. No caso de POST(geralmente usada para cadastrar algo), você ira acrescentar um atributo a mais
             na assinatura da função, esse atributo corresponde ao body da requisição, exemplo:
             
             this.ProdutoProvider.cadastrarProduto(produto)
                 .then(response => console.log(response)) // Aqui você verifica o retorno e trata da maneira que achar melhor
                 .catch(error => console.log(error)) // Mesma coisa dita acima
             e lá no ProdutoProvider fica mais ou menos assim:
              cadastrarProduto(produto: any){
                return new Promise((resolve, reject) =>{
                  let url = this.API_URL + 'api/produtos';
                  this.http.post(url, produto).subscribe((result: any) => {
                    resolve(result.json());
                  }, (error) => {
                    reject(error.json());
                  });
              });
        */
        getProdutos(): void {
          this.ProdutoProvider.getProdutos()
            .then(produtos => this.produtos = produtos)
            .catch(error => console.log(error))
        }

        addProduto(name: string): void {
          this.alert= '';
          name = name.trim();
          // if (!name) { this.alert="Dados inválidos!";return; }
            // this.ProdutoProvider.addProduto({nome: name} as any)
            //   .subscribe(produto =>
            //     this.produtos.push(produto)
            //   );
            //   this.alert = "Produto cadastrado com sucesso!";
          // }
        }

        removerMensagem(){
          this.alert ='';
        }
      }