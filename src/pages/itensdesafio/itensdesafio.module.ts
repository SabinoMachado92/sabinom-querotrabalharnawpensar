import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ItensdesafioPage } from './itensdesafio';

@NgModule({
  declarations: [
    ItensdesafioPage,
  ],
  imports: [
    IonicPageModule.forChild(ItensdesafioPage),
  ],
  exports: [
    ItensdesafioPage,
  ]
})
export class ItensdesafioPageModule {}
