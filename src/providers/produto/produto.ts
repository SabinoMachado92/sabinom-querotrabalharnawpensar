import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { error } from '@firebase/database/dist/esm/src/core/util/util';

@Injectable()
export class ProdutoProvider {

  API_URL = 'http://private-85ad8-querotrabalharnawpensar.apiary-mock.com/'

  constructor(public http: Http) {
   
  }  
    
  getProdutos(){
    return new Promise((resolve, reject) =>{

      let url = this.API_URL + 'api/produtos';

      this.http.get(url)
      .subscribe((result: any) => {
        resolve(result.json());
      },
    (error) => {
      reject(error.json());
    });
    });
  }

  getId(id: number){
    return new Promise((resolve, reject) =>{

      let url = this.API_URL + 'api/produtos/' + id;

      this.http.get(url)
      .subscribe((result: any) => {
        resolve(result.json());
      },
    (error) => {
      reject(error.json());
    });
    });
  }

  




 
  


}